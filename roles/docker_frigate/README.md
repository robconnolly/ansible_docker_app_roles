Ansible Docker Frigate Role
=========

An Ansible role to install and configure a Frigate NVR server

Requirements
------------

None

Role Variables
--------------

Dependencies
------------

None.

Example Playbook
----------------

User the role as follows:

```yaml
  - hosts: servers
    roles:
      - role: docker_frigate
```

License
-------

AGPLv3

Author Information
------------------

Rob Connolly [https://webworxshop.com](https://webworxshop.com)
