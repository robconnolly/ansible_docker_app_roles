Ansible Docker Tvheadend Role
=========

An Ansible role to install and configure a linuxserver/tvheadend server

Requirements
------------

None

Role Variables
--------------

* nfs_server_addr - IP address of the NFS server
* recordings_share - Path of the NFS share for recordings

Dependencies
------------

None.

Example Playbook
----------------

User the role as follows:

```yaml
  - hosts: servers
    vars:
      - nfs_server_addr: 192.168.1.1
      - recordings_share: /storage/recordings
    roles:
      - role: docker_tvheadend
```

License
-------

AGPLv3

Author Information
------------------

Rob Connolly [https://webworxshop.com](https://webworxshop.com)
