Ansible Docker Jellyfin Role
=========

An Ansible role to install and configure a linuxserver/jellyfin server

Requirements
------------

None

Role Variables
--------------

* nfs_server_addr - IP address of the NFS server
* media_share - Path of the NFS share for media
* music_share - Path of the NFS share for music
* docker_share - Path of the NFS share for docker data

Dependencies
------------

None.

Example Playbook
----------------

User the role as follows:

```yaml
  - hosts: servers
    vars:
      - nfs_server_addr: 192.168.1.1
      - recordings_share: /storage/media
      - docker_share: /storage/docker
    roles:
      - role: docker_jellyfin
```

License
-------

AGPLv3

Author Information
------------------

Rob Connolly [https://webworxshop.com](https://webworxshop.com)
