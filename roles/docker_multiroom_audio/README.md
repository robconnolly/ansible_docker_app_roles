Ansible Docker Jellyfin Role
=========

An Ansible role to install and configure a multiroom audio system based on
Mopidy and Snapcast, backed by Jellyfin (not included, see docker_jellyfin
role).

Requirements
------------

None

Role Variables
--------------

* nfs_server_addr - IP address of the NFS server
* music_share - Path of the NFS share for music
* docker_share - Path of the NFS share for docker data
* jellyfin_host - Hostname of the jellyfin server
* jellyfin_user - Username for jellyfin
* jellyfin_passwd - Password for jellyfin

Dependencies
------------

None.

Example Playbook
----------------

User the role as follows:

```yaml
  - hosts: servers
    vars:
      - nfs_server_addr: 192.168.1.1
      - music_share: /storage/music
      - docker_share: /storage/docker
    roles:
      - role: docker_multiroom_audio
```

License
-------

AGPLv3

Author Information
------------------

Rob Connolly [https://webworxshop.com](https://webworxshop.com)
