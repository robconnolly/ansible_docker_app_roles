Ansible Docker SWAG Proxy Role
=========

An Ansible role to install and configure a linuxserver/swag reverse proxy.

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None.

Example Playbook
----------------

User the role as follows:

```yaml
  - hosts: servers
    roles:
      - role: docker_swag_proxy
```

License
-------

AGPLv3

Author Information
------------------

Rob Connolly [https://webworxshop.com](https://webworxshop.com)
